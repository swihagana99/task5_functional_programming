package singingCompetition;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SingerDetails {
	private int singerPosition;
    private String singerName;
    private int vocalSkill;
    private int stagePresence;
    private int audienceEngagement;
    private int totalScore;
    private double averageScore;
    
 public SingerDetails(int singerPosition, String singerName, int vocalSkill, int stagePresence, int audienceEngagement, int totalScore, double averageScore) {
    	this.singerPosition = singerPosition;
        this.singerName = singerName;
        this.vocalSkill = vocalSkill;
        this.stagePresence = stagePresence;
        this.audienceEngagement = audienceEngagement;
        this.totalScore = totalScore;
        this.averageScore = averageScore;
    }

    public int compareTo(SingerDetails singerDetails) {
    	return ((Integer) totalScore).compareTo(singerDetails.totalScore);
    }

    public String toString() {
    	return String.format("%-5d%-30s%10d%10d%10d%10d%10.2f", singerPosition, singerName, vocalSkill, stagePresence, audienceEngagement, totalScore, averageScore);
    }

    public int getSingerPosition() {
    	return singerPosition;
    }

    public void setSingerPosition(int singerPosition) {
    	this.singerPosition = singerPosition;
    }

    public String getSingerName() {
    	return singerName;
    }

    public void setSingerName(String singerName) {
    	this.singerName = singerName;
    }
    
    public int getVocalSkill() {
    	return vocalSkill;
    }

    public void setVocalSkill(int vocalSkill) {
    	this.vocalSkill = vocalSkill;
    }

    public int getStagePresence() {
    	return stagePresence;
    }

    public void setStagePresence(int stagePresence) {
    	this.stagePresence = stagePresence;
    }

    public int getAudienceEngagement() {
    	return audienceEngagement;
    }

    public void setAudienceEngagement(int audienceEngagement) {
    	this.audienceEngagement = audienceEngagement;
    }

    public int getTotalScore() {
    	return totalScore;
    }
    
    public void setTotalScore(int totalScore) {
    	this.totalScore = totalScore;
    }

    public double getAverageScore() {
    	return averageScore;
    }

    public void setAverageScore(double averageScore) {
    this.averageScore = averageScore;
    }
}
