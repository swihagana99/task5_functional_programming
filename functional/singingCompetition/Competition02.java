package singingCompetition;

import java.util.Arrays;
import java.util.List;

import rugby.Club;

public class Competition02 {
	public static void main(String[] args) {
        List<SingerDetails> table = Arrays.asList(
                new SingerDetails(1, "John Smith", 8, 9, 7, 24, 8.0),
                new SingerDetails(2, "Bath", 22, 16, 0, 6, 625.0),
                new SingerDetails(3, "Leicester Tigers", 22, 15, 1, 6, 453.0),
                new SingerDetails(4, "Saracens", 22, 14, 1, 7, 664.0),
                new SingerDetails(5, "Exeter Chiefs", 22, 14, 0, 8, 663.0),
                new SingerDetails(6, "Wasps", 22, 11, 2, 9, 672.0),
                new SingerDetails(7, "Sale Sharks", 22, 11, 0, 11, 497.0),
                new SingerDetails(8, "Harlequins", 22, 10, 0, 12, 444.0),
                new SingerDetails(9, "Gloucester", 22, 9, 1, 12, 553.0),
                new SingerDetails(10, "London Irish", 22, 7, 1, 14, 442.0),
                new SingerDetails(11, "Newcastle Falcons", 22, 5, 1, 16, 475.0),
                new SingerDetails(12, "London Welsh", 22, 0, 0, 22, 223.0)
        );

	    table.stream().forEach(x -> System.out.println(x));
	    System.out.println();
	    table.parallelStream().forEach(System.out::println);
	  
	// Calculate and print the average vocal skill across all singers
	double averageVocalSkill = table.stream().mapToInt(SingerDetails::getVocalSkill).average().orElse(0.0);
	System.out.println("");
	System.out.println("Average Vocal Skill: " + averageVocalSkill);

	// Calculate and print the sum of stage presence for all singers
	int totalStagePresence = table.stream().mapToInt(SingerDetails::getStagePresence).sum();
	System.out.println("");
	System.out.println("Total Stage Presence: " + totalStagePresence);
	}
}
